// 获取主导航高度，重置#main 上边距
var main = $('#main');
var headerHeight = $('#main-navbar').outerHeight();
var footerHeight = $('#footer-navbar').outerHeight();
main.css('min-height', $(window).height() - headerHeight - footerHeight);
main.css('margin-top', headerHeight);
