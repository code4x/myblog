panelShow();
getValues();


function panelShow() {
    document.getElementsByClassName('tab-pane')[0].classList.add('active');
    document.getElementById('my-tab').getElementsByClassName('nav-link')[0].classList.add('active');

}

function getValues() {
    var data1 = 1; //技能攻击力
    var data2 = 0; //属性强化
    var data3 = 0; //百分比攻击
    var data4 = 0; //附加伤害
    var data5 = 0; //额外 黄字
    var data6 = 0; //额外 爆伤
    var data7 = 0; //百分比力智
    var data8 = 0; //最终 伤害
    var line = document.getElementsByClassName('cs-input');
    for (var i = 0; i < line.length; i++) {
        var selected = line[i].getElementsByTagName('select');
        var input = line[i].getElementsByTagName('input');
        var selectedVal = selected[0].value;
        var inputVal = input[0].value;
        switch (selectedVal) {
            case '0':
                // alert('请重新选择');
                break;
            case '1':
                data1 = data1 * Number(inputVal / 100 + 1);
                break;
            case '2':
                data2 = data2 + Number(inputVal);
                break
            case '3':
                data3 = data3 + Number(inputVal);
                break
            case '4':
                data4 = data4 + Number(inputVal);
                break
            case '5':
                data5 = data5 + Number(inputVal);
                break
            case '6':
                data6 = data7 + Number(inputVal);
                break
            case '7':
                data7 = data7 + Number(inputVal);
                break
            case '8':
                data8 = data8 + Number(inputVal);
                break
        }

        var inputVal = input[0].value;
        console.log(typeof (inputVal));
    }
    document.getElementById('total1').value = data1;
    document.getElementById('total2').value = data2;
    document.getElementById('total3').value = data3;
    document.getElementById('total4').value = data4;
    document.getElementById('total5').value = data5;
    document.getElementById('total6').value = data6;
    document.getElementById('total7').value = data7;
    document.getElementById('total8').value = data8;
}


function addLine() {
    var ele = document.getElementsByClassName('cs-input')[0].outerHTML;
    var eleContent = new DOMParser().parseFromString(ele, 'text/html').body.childNodes[0];
    var tabPanels = document.getElementsByClassName('tab-pane');
    var activePanel = null;
    for (var i = 0; i < tabPanels.length; i++) {
        if (tabPanels[i].classList.contains('active')) {
            activePanel = tabPanels[i];

        }
    }


    activePanel.append(eleContent);
}

//获取指定class的父节点
function getParents(element, className) {
    var that = this;
    var returnParentElement = null;

    function getParentNode(element, className) {
        if (that.isElement(element)) {
            if (element && element.classList.contains(className) && element.tagName.toLowerCase() != "body") {
                returnParentElement = element;
            } else {
                getParentNode(element.parentElement, className);
            }
        }
    }

    getParentNode(element, className);

    return returnParentElement;
}